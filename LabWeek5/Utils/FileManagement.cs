using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Platform.Storage;
using LabWeek5.Models;

namespace LabWeek5.Utils;

public class FileManagement(TopLevel? topLevel)
{
    private readonly LogManagement _logManagement = new();
    private string _pathFile;
    private IReadOnlyList<IStorageFile> _file;
    private ObservableCollection<User> _userFromFile = new ();

    private async Task ExportFile()
    {
        try
        {
            var selectedFolder = await topLevel?.StorageProvider.OpenFolderPickerAsync(new FolderPickerOpenOptions())!;

            var path = selectedFolder.First().Path;

            var filePath = Path.Combine(path.AbsolutePath, "List-Users.txt");
            _pathFile = filePath;
        }
        catch (Exception e)
        {
            throw new Exception("Export failed");
        }
    }

    private async Task ImportFile()
    {
        try
        {
            var selectedFileOptions = new FilePickerOpenOptions
            {
                FileTypeFilter = new [] {FilePickerFileTypes.TextPlain}
            };
            
            _file = await topLevel?.StorageProvider.OpenFilePickerAsync(selectedFileOptions)!;;
            
        }
        catch (Exception e)
        {
            throw new Exception("Import failed");
        }
    }
    
    
    
    public async Task ReadFile()
    {
        try
        {
            _userFromFile.Clear();
            await ImportFile();
            
            await using var stream = await _file.First().OpenReadAsync();
            using (StreamReader reader = new StreamReader(stream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!ValidatingData(line)) continue;
                    User user = FormatData(line);
                    _userFromFile.Add(user);

                }
            }
        }
        catch (Exception e)
        {
            throw new Exception("Read file failed");
        }
    }
    
    public async Task WriteFile(ObservableCollection<User> users)
    {
        
        try
        {
            if (users.Count == 0)
            {
                throw new Exception("Data is empty");
            }
            
            await ExportFile();
            
            StringBuilder stringUsers = new();

            foreach (User user in users)
            {
                stringUsers.Append($"{user.ToString()}\n");
            }
            
            using (StreamWriter writer = new StreamWriter(_pathFile))
            {
                writer.Write(stringUsers);
            }

            Console.WriteLine("File created successfully!");
            
        }
        catch (Exception e)
        {
            var msg = $"Error to export file: {e.Message}";
            Console.WriteLine(msg);
            _logManagement.AddLog(msg);
        }

    }

    private bool ValidatingData(string line)
    {
        bool validator = true;
        string[] datas = line.Trim().Split(",");

        if (datas.Length > 3) validator = false;

        for (int i = 0; i < datas.Length; i++)
        {
            if (i == 2)
            {
                validator = Validator.ValidateEmail(datas[i]);
                continue;
            };

            validator = Validator.ValidatorName(datas[i]);
        }

        return validator;
    }

    private User FormatData(string line)
    {
        line.Trim();
        string[] datas = line.Split(",");

        return new User(datas[0], datas[1], datas[2]);
    }

    public ObservableCollection<User> GetUsersFromFile()
    {
        return _userFromFile;
    }
}